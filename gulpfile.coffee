gulp = require 'gulp'
jade = require 'gulp-jade'
less = require 'gulp-less'
rimraf = require 'rimraf'
coffee = require 'gulp-coffee'

SRC = __dirname + '/web-assets/src'
BUILD = __dirname + '/web-assets/build'

gulp.task 'default', ['clean', 'jade', 'coffee', 'less', 'assets'], ->

gulp.task 'clean', [], (cb) ->
	rimraf BUILD, cb

gulp.task 'jade', ['clean'], ->
	gulp.src SRC + '/jade/*.jade'
		.pipe jade { pretty: true }
		.pipe gulp.dest BUILD

gulp.task 'less', ['clean'], ->
	gulp.src SRC + '/less/*.less'
		.pipe less {}
		.pipe gulp.dest BUILD

gulp.task 'coffee', ['clean'], ->
	gulp.src SRC + '/coffee/*.coffee'
		.pipe coffee {bare: true}
		.pipe gulp.dest BUILD

gulp.task 'assets', ['clean'], ->
	gulp.src SRC + '/vendor/**/*'
		.pipe gulp.dest BUILD + '/vendor'
