var ACCELERATION_THRESHOLD, MIC_THRESHOLD, RESTORE_TIME, accel, context, current, getContext, initMic, isMoved, last, micNode, onMicConnected, registered, timeoutId;

ACCELERATION_THRESHOLD = 3.0;

MIC_THRESHOLD = 50;

RESTORE_TIME = 600;

getContext = function() {
  var context;
  context = window.AudioContext || window.webkitAudioContext;
  if (context == null) {
    alert('音声取得ができません！ AudioContext not found');
  }
  return new context();
};

context = getContext();

initMic = function(onConnected) {
  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || null;
  if (!navigator.getUserMedia) {
    alert('音声取得ができません！ getUserMedia not found');
  }
  return navigator.getUserMedia({
    audio: true
  }, onConnected, function(e) {
    return alert('マイク接続時にエラーが発生しました! : ' + e);
  });
};

micNode = null;

timeoutId = null;

registered = false;

onMicConnected = function(mic) {
  var analyserNode, threshold_overed, threshold_overed_time;
  micNode = context.createMediaStreamSource(mic);
  analyserNode = context.createAnalyser();
  micNode.connect(analyserNode);
  new Visualizer($('#analyser'), analyserNode);
  threshold_overed = false;
  threshold_overed_time = -1;
  return timeoutId = setInterval(function() {
    var data, i, len, maximum, y;
    data = new Uint8Array(analyserNode.fftSize);
    analyserNode.getByteTimeDomainData(data);
    maximum = 0;
    for (i = 0, len = data.length; i < len; i++) {
      y = data[i];
      maximum = Math.max(Math.abs(y - 128), maximum);
    }
    if (registered) {
      return;
    }
    if (!threshold_overed && maximum > MIC_THRESHOLD) {
      threshold_overed = true;
      threshold_overed_time = performance.now();
      $('#flash').removeClass('not-speaking');
      $('#flash').addClass('speaking');
      return window.LINE_COLOR = '#990000';
    } else if (maximum > MIC_THRESHOLD) {
      return threshold_overed_time = performance.now();
    } else if (threshold_overed && maximum <= MIC_THRESHOLD && performance.now() > threshold_overed_time + RESTORE_TIME) {
      $('#flash').removeClass('speaking');
      $('#flash').addClass('not-speaking');
      threshold_overed = false;
      window.LINE_COLOR = '#330000';
      return window.location.href = 'http://tabelog.com/hokkaido/A0101/A010103/1046884/';
    }
  }, 32);
};

current = 0;

last = 0;

accel = 0;

isMoved = function(acc) {
  var delta;
  last = current;
  current = Math.sqrt(acc.x * acc.x + acc.y * acc.y + acc.z * acc.z);
  delta = current - last;
  accel = accel * 0.9 + delta;
  return accel > 10;
};

$(function() {
  window.addEventListener('devicemotion', function(e) {
    if (isMoved(e.acceleration) && !registered) {
      try {
        $('#test').click();
        $('#registered').fadeIn(100);
        return registered = true;
      } catch (_error) {
        e = _error;
        return alert(e);
      }
    }
  });
  $('#bg').fadeIn(1500, function() {
    return initMic(onMicConnected);
  });
  return $('#test').click(function() {
    var a, e;
    try {
      a = document.getElementById('a');
      a.muted = false;
      a.volume = 1;
      return a.play();
    } catch (_error) {
      e = _error;
      return alert(e);
    }
  });
});
