ACCELERATION_THRESHOLD = 3.0
MIC_THRESHOLD = 50
RESTORE_TIME = 600

getContext = ->
	context = window.AudioContext or window.webkitAudioContext
	alert('音声取得ができません！ AudioContext not found') if not context?
	new context()
context = getContext()

initMic = (onConnected) ->
	navigator.getUserMedia =
		navigator.getUserMedia or navigator.webkitGetUserMedia or navigator.mozGetUserMedia or navigator.msGetUserMedia or null
	alert('音声取得ができません！ getUserMedia not found') if not navigator.getUserMedia

	navigator.getUserMedia(
		{ audio: true },
		onConnected,
		(e) -> alert('マイク接続時にエラーが発生しました! : ' + e)
	)

micNode = null
timeoutId = null
registered = false
onMicConnected = (mic) ->
	micNode = context.createMediaStreamSource mic
	#micNode.connect context.destination

	analyserNode = context.createAnalyser()
	micNode.connect analyserNode
	new Visualizer $('#analyser'), analyserNode

	threshold_overed = false
	threshold_overed_time = -1

	timeoutId = setInterval ->
		data = new Uint8Array analyserNode.fftSize
		analyserNode.getByteTimeDomainData data
		maximum = 0
		for y in data
			maximum = Math.max(Math.abs(y - 128), maximum)

		return if registered

		if not threshold_overed and maximum > MIC_THRESHOLD
			threshold_overed = true
			threshold_overed_time = performance.now()
			$('#flash').removeClass 'not-speaking'
			$('#flash').addClass 'speaking'
			window.LINE_COLOR = '#990000'
		else if maximum > MIC_THRESHOLD
			threshold_overed_time = performance.now()
		else if threshold_overed and maximum <= MIC_THRESHOLD and performance.now() > threshold_overed_time + RESTORE_TIME
				$('#flash').removeClass 'speaking'
				$('#flash').addClass 'not-speaking'
				threshold_overed = false
				window.LINE_COLOR = '#330000'
				window.location.href = 'http://tabelog.com/hokkaido/A0101/A010103/1046884/' # 検索対象に移動

	, 32

current = 0
last = 0
accel = 0
isMoved = (acc) ->
	last = current
	current = Math.sqrt acc.x * acc.x + acc.y * acc.y + acc.z * acc.z
	delta = current - last
	accel = accel * 0.9 + delta
	return accel > 10

$ ->
	window.addEventListener 'devicemotion', (e) ->
		#acc = Math.abs(e.acceleration.x) + Math.abs(e.acceleration.y) + Math.abs(e.acceleration.z)

		if isMoved(e.acceleration) and not registered
			try
				#$('audio').get(0).play()
				$('#test').click()
				$('#registered').fadeIn 100
				registered = true
			catch e
				alert e

	$('#bg').fadeIn 1500, ->
		initMic onMicConnected

	$('#test').click ->
		try
			a = document.getElementById 'a'
			a.muted = false
			a.volume = 1
			a.play()
		catch e
			alert e
