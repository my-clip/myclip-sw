window.LINE_COLOR = '#330000';

var Visualizer = function ($canvas, node) {
    var CLOCK = 32;

    var $window = $(window);
    var ctx = $canvas.get(0).getContext('2d');
    var width, height;

    var onResize = function () {
        width = Math.floor($window.width());
        height = Math.floor($window.height() * 0.8);
        $canvas.css({
            width: width + 'px',
            height: height + 'px'
        });
    };

    var setSize = function () {
        $canvas.attr({
            width: width,
            height: height
        });
    };

    var drawData = function (data) {
        var Y_MAX = height / 4;
        setSize();
        ctx.strokeStyle = window.LINE_COLOR;
		ctx.lineWidth = 2;
		ctx.lineJoin = 'round';
        var y, y_percent;
        for (var i = 0; i < width; i++) {
			y_percent = (data[i] - 128.0) / 256.0 * 2.0;
            y = Math.floor(y_percent * Y_MAX + height / 2);
			if (!$('body').hasClass('speaking') && y_percent > -0.05 && y_percent < 0.05) { y = height / 2; }
            if (i == 0) {
                ctx.moveTo(i, y);
            } else {
                ctx.lineTo(i, y);
            }
        }
        ctx.stroke();
    };

    setInterval(function () {
        var data = new Uint8Array(width);
        // node.getByteFrequencyData(data);
        node.getByteTimeDomainData(data);
        drawData(data);
    }, CLOCK);

    $window.on('resize', onResize);
    onResize();
};
